<?php
/**
 * Qwik Pik.
 *
 * This file adds functions to the Qwik Pik Theme.
 *
 * @package Qwik Pik
 * @author  Mauricio Alvarez
 * @license GPL-2.0+
 * @link    http://designnify.com/
 */

//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Setup Theme
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'genesis-sample', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'genesis-sample' ) );

//* Add Image upload and Color select to WordPress Theme Customizer
require_once( get_stylesheet_directory() . '/lib/customize.php' );

//* Include Customizer CSS
include_once( get_stylesheet_directory() . '/lib/output.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Genesis Sample' );
define( 'CHILD_THEME_URL', 'http://www.studiopress.com/' );
define( 'CHILD_THEME_VERSION', '2.2.4' );

//* Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', 'genesis_sample_enqueue_scripts_styles' );
function genesis_sample_enqueue_scripts_styles() {

	wp_enqueue_style( 'genesis-sample-fonts', '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700|Open+Sans:400,700', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'dashicons' );

	wp_enqueue_script( 'genesis-sample-responsive-menu', get_stylesheet_directory_uri() . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0', true );
	$output = array(
		'mainMenu' => __( 'Menu', 'genesis-sample' ),
		'subMenu'  => __( 'Menu', 'genesis-sample' ),
	);
	wp_localize_script( 'genesis-sample-responsive-menu', 'genesisSampleL10n', $output );
	
	//* Enqueue Adobe Edge Fonts (Cooper Black Std)
	wp_register_script( 'my_adobe_edge_fonts', '//use.edgefonts.net/cooper-black-std.js' );
    wp_enqueue_script( 'my_adobe_edge_fonts' );

}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

//* Add Accessibility support
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for woocommerce
add_theme_support( 'genesis-connect-woocommerce' );

//* Remove the header right widget area
unregister_sidebar( 'header-right' );

//* Add support for after entry widget
add_theme_support( 'genesis-after-entry-widget-area' );

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 2 );

//* Add Image Sizes
add_image_size( 'featured-image', 720, 400, TRUE );

//* Remove the secondary sidebar
unregister_sidebar( 'sidebar-alt' );

//* Remove unnecessary layouts
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );
genesis_unregister_layout( 'sidebar-content-sidebar' );

//* Enable shortcodes in widgets
add_filter('widget_text', 'do_shortcode');

//* Reduce the secondary navigation menu to one level depth
/*
add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );
function genesis_sample_secondary_menu_args( $args ) {

	if ( 'secondary' != $args['theme_location'] ) {
		return $args;
	}

	$args['depth'] = 1;

	return $args;

}
*/

//* Modify size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'genesis_sample_author_box_gravatar' );
function genesis_sample_author_box_gravatar( $size ) {

	return 90;

}

//* Modify size of the Gravatar in the entry comments
add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );
function genesis_sample_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;

	return $args;

}

//* Customize the credits
add_filter( 'genesis_footer_creds_text', 'sp_footer_creds_text' );
function sp_footer_creds_text() {
	echo '<div class="creds"><p>';
	echo 'Copyright &copy; ';
	echo date('Y');
	echo ' &middot; Qwik Pik &middot; All rights reserved &middot; Built by Mauricio Alvarez';
	echo '</p></div>';
}

//* Allow SVG through WordPress Media Uploader
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


//* Register widget areas
genesis_register_sidebar( array(
	'id'          => 'opening-hours',
	'name'        => __( 'Opening Hours', 'qwik-pik' ),
	'description' => __( 'This is the top section where we include the opening hours', 'qwik-pik' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home Top', 'qwik-pik' ),
	'description' => __( 'This is the home page top section.', 'qwik-pik' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-middle',
	'name'        => __( 'Home Middle', 'qwik-pik' ),
	'description' => __( 'This is the home page middle section.', 'qwik-pik' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom',
	'name'        => __( 'Home Bottom', 'qwik-pik' ),
	'description' => __( 'This is the home page bottom section.', 'qwik-pik' ),
) );

//* Hook widget areas
add_action( 'genesis_before', 'page_widgets' );
function page_widgets() {

	genesis_widget_area( 'opening-hours', array(
		'before' => '<section class="opening-hours"><div class="wrap">',
		'after'  => '</div></section>',
	) );

}